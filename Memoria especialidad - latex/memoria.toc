\boolfalse {citerequest}\boolfalse {citetracker}\boolfalse {pagetracker}\boolfalse {backtracker}\relax 
\defcounter {refsection}{0}\relax 
\select@language {spanish}
\contentsline {chapter}{Resumen}{\es@scroman {iii}}{section*.1}
\contentsline {chapter}{\numberline {1}Introducci\IeC {\'o}n General}{1}{chapter.5}
\contentsline {section}{\numberline {1.1}Contexto y justificaci\IeC {\'o}n}{1}{section.6}
\contentsline {section}{\numberline {1.2}Objetivos}{2}{section.12}
\contentsline {section}{\numberline {1.3}Alcance del proyecto}{2}{section.13}
\contentsline {section}{\numberline {1.4}Requerimientos}{3}{section.14}
\contentsline {chapter}{\numberline {2}Introducci\IeC {\'o}n Espec\IeC {\'\i }fica}{5}{chapter.27}
\contentsline {section}{\numberline {2.1}Resumen del trabajo desarrollado}{5}{section.28}
\contentsline {subsection}{\numberline {2.1.1}Esquema general}{5}{subsection.29}
\contentsline {subsubsection}{IDE para dise\IeC {\~n}o de interfaz}{5}{section*.31}
\contentsline {subsubsection}{Dise\IeC {\~n}o de Aplicaci\IeC {\'o}n}{6}{section*.33}
\contentsline {subsubsection}{Componentes Hardware}{6}{section*.37}
\contentsline {subsection}{\numberline {2.1.2}Esquema de software}{6}{subsection.38}
\contentsline {subsubsection}{Bibliotecas de componentes visuales}{7}{section*.39}
\contentsline {subsubsection}{Conversor de pantallas}{8}{section*.41}
\contentsline {section}{\numberline {2.2}Herramientas utilizadas}{9}{section.43}
\contentsline {subsection}{\numberline {2.2.1}HDMI y Display Link}{9}{subsection.44}
\contentsline {subsubsection}{HDMI}{9}{section*.45}
\contentsline {subsubsection}{Display Link}{10}{section*.47}
\contentsline {subsection}{\numberline {2.2.2}USB-OTG}{10}{subsection.50}
\contentsline {subsection}{\numberline {2.2.3}Proyecto CIAA}{11}{subsection.52}
\contentsline {subsubsection}{EDU-CIAA}{12}{section*.53}
\contentsline {subsection}{\numberline {2.2.4}IDE para dise\IeC {\~n}o de interfaces gr\IeC {\'a}ficas de usuario}{13}{subsection.56}
\contentsline {chapter}{\numberline {3}Dise\IeC {\~n}o e Implementaci\IeC {\'o}n}{15}{chapter.58}
\contentsline {section}{\numberline {3.1}Planificacion de tareas}{15}{section.59}
\contentsline {section}{\numberline {3.2}USB-HDMI}{16}{section.61}
\contentsline {section}{\numberline {3.3}Implementacion de biblioteca libdlo en EDU-CIAA}{17}{section.63}
\contentsline {section}{\numberline {3.4}Desarrollo de formas b\IeC {\'a}sicas}{19}{section.68}
\contentsline {subsection}{\numberline {3.4.1}Dise\IeC {\~n}o e implementaci\IeC {\'o}n}{19}{subsection.69}
\contentsline {subsection}{\numberline {3.4.2}Intrucciones de uso}{20}{subsection.70}
\contentsline {section}{\numberline {3.5}Desarrollo de objetos visuales}{22}{section.73}
\contentsline {subsection}{\numberline {3.5.1}Dise\IeC {\~n}o e implementaci\IeC {\'o}n}{23}{subsection.75}
\contentsline {subsection}{\numberline {3.5.2}Instrucciones de uso}{24}{subsection.78}
\contentsline {subsubsection}{paintWindows\_t}{25}{section*.84}
\contentsline {subsubsection}{paintLabel\_t}{25}{section*.85}
\contentsline {subsubsection}{paintTextBox\_t}{26}{section*.86}
\contentsline {subsubsection}{paintButton\_t}{27}{section*.87}
\contentsline {subsubsection}{paintProgressBar\_t}{27}{section*.88}
\contentsline {subsubsection}{paintTerminal\_t}{28}{section*.89}
\contentsline {subsubsection}{paintIndicator\_t}{29}{section*.90}
\contentsline {subsubsection}{paintGraphicXY\_t}{30}{section*.91}
\contentsline {section}{\numberline {3.6}Conversor de pantallas}{31}{section.92}
\contentsline {subsection}{\numberline {3.6.1}Dise\IeC {\~n}o e implementaci\IeC {\'o}n}{31}{subsection.93}
\contentsline {subsection}{\numberline {3.6.2}Instrucciones de uso}{32}{subsection.99}
\contentsline {subsubsection}{Plug-in de eclipse}{32}{section*.100}
\contentsline {subsubsection}{Pantallas Qt Creator}{33}{section*.103}
\contentsline {chapter}{\numberline {4}Ensayos y Resultados}{35}{chapter.105}
\contentsline {section}{\numberline {4.1}Ensayos de formas b\IeC {\'a}sicas}{35}{section.106}
\contentsline {subsubsection}{Detecci\IeC {\'o}n y pantalla completa}{35}{section*.107}
\contentsline {subsubsection}{Barras horizontales y verticales}{35}{section*.109}
\contentsline {subsubsection}{P\IeC {\'\i }xeles}{36}{section*.111}
\contentsline {subsubsection}{L\IeC {\'\i }neas}{37}{section*.113}
\contentsline {subsubsection}{C\IeC {\'\i }rculos}{37}{section*.115}
\contentsline {subsubsection}{Rect\IeC {\'a}ngulos y copia de bloques de pantalla}{38}{section*.117}
\contentsline {subsubsection}{Fuentes tipogr\IeC {\'a}ficas}{38}{section*.119}
\contentsline {section}{\numberline {4.2}Ensayos de componentes de ventana}{39}{section.121}
\contentsline {subsubsection}{Etiqueta de texto (label)}{39}{section*.122}
\contentsline {subsubsection}{Caja de texto (TextBox)}{39}{section*.124}
\contentsline {subsubsection}{Bot\IeC {\'o}n (button)}{40}{section*.125}
\contentsline {subsubsection}{Indicador de estado (status indicator)}{40}{section*.126}
\contentsline {subsubsection}{Barra de progreso (progress bar)}{40}{section*.128}
\contentsline {subsubsection}{Terminal}{40}{section*.130}
\contentsline {subsubsection}{Gr\IeC {\'a}fica X vs Y}{40}{section*.132}
\contentsline {section}{\numberline {4.3}Ensayos de generaci\IeC {\'o}n de interfaz}{41}{section.134}
\contentsline {subsubsection}{Dise\IeC {\~n}o de pantalla}{41}{section*.135}
\contentsline {chapter}{\numberline {5}Conclusiones}{43}{chapter.138}
\contentsline {section}{\numberline {5.1}Conclusiones generales }{43}{section.139}
\contentsline {section}{\numberline {5.2}Pr\IeC {\'o}ximos pasos}{44}{section.140}
\contentsline {chapter}{Bibliograf\'{\i }a}{45}{appendix*.141}
